import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class TelaCliente extends JFrame{

	public ChatClient cliente;
	public ServerObserver serverObserver;
	public JTextArea mensagens;
	
	public TelaCliente(){
		
		serverObserver = new ServerObserver(this);
		String nome = JOptionPane.showInputDialog(this, "Seu Nome: ");
		GridLayout layout = new GridLayout();
		layout.setColumns(1);
		layout.setRows(3);
		
		this.setSize(400, 400);
		this.setTitle(nome);
		this.setVisible(true);
		this.setLayout(layout);
		
		this.mensagens = new JTextArea();
		mensagens.setEditable(false);
		mensagens.setSize(200,200);
		mensagens.setColumns(20);
		mensagens.setVisible(true);
		
		JTextField campo = new JTextField();
		campo.setSize(200,200);
		campo.setText("ola");
		campo.setVisible(true);

		JButton button = new JButton();
		button.setText("Enviar");
		button.setVisible(true);
		button.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String mensagem = campo.getText();
				try {
					cliente.enviarMensagem(cliente.getNome() + ": " + mensagem);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		this.add(mensagens);
		this.add(campo);
		this.add(button);
		
		
		try {
			System.out.println("comecou a thread");
			cliente = new ChatClient(nome, serverObserver);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("Continuou depois da thread");
		
	}
	
	public static void main(String[] args) {
		new TelaCliente();
	}
	
}