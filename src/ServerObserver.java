import java.util.Observable;
import java.util.Observer;

public class ServerObserver implements Observer{

	private TelaCliente tela;
	
	public ServerObserver(TelaCliente tela) {
		this.tela = tela;
	}
	
	@Override
	public void update(Observable observable, Object mensagem) {
		String m = tela.mensagens.getText();
		tela.mensagens.setText(m + mensagem  + "\n");
	}
	
}
