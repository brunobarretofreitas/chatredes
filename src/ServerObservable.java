import java.util.Observable;
import java.util.Observer;

public class ServerObservable extends Observable{

	private Observer observer;
	
	public ServerObservable(Observer observer){
		this.observer = observer;
	}
	
	public void notifyObservers(String mensagem) {
		this.observer.update(this, mensagem);
	}
	
}
