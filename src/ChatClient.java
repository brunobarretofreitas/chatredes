import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import javax.swing.JOptionPane;
public class ChatClient{
	
	private String nomeCliente;
	private String mensagem;
	public static Observer observer;
	public static DataOutputStream out;
	public static DataInputStream in;
	public static Socket s;	
	
	public ChatClient(String nomeCliente, Observer observer) throws UnknownHostException, IOException{
		this.nomeCliente = nomeCliente;
		this.observer = observer;
		int serverPort = 7896;
		
		String ip = JOptionPane.showInputDialog("Qual o IP da Maquina?");
		s = new Socket(ip, serverPort);
		out = new DataOutputStream( s.getOutputStream());
		in = new DataInputStream(s.getInputStream());
		
		new MensagemListener(in, observer).start();
		System.out.println("entenread");
	}
	
	public String getNome() {
		return this.nomeCliente;
	}
	
	public void enviarMensagem(String mensagem) throws IOException{
		this.out.writeUTF(mensagem);
	}
}

	class MensagemListener extends Thread{
		
		public DataInputStream in;
		public ServerObservable serverObservable;
		public Observer observer;
		
		public MensagemListener(DataInputStream in, Observer observer) {
			this.in = in;
			this.serverObservable = new ServerObservable(observer);
		}
		
		@Override
		public void run(){
			while(true){
				try {
					//Esperando receber alguma mensagem do Servidor;
					String mensagem = in.readUTF();
					this.serverObservable.notifyObservers(mensagem);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
}