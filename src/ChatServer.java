import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatServer{
	
	public static List<Connection> clientes = new ArrayList<Connection>();
	private Observer observer;
	
	public ChatServer(Observer observer){
		this.observer = observer;
	}
	
	public static void main (String args[]) {
		
		try{
			System.out.println("Servidor Ligado");
			int serverPort = 7896; // the server port
			ServerSocket listenSocket = new ServerSocket(serverPort);
			while(true) {
				Socket clientSocket = listenSocket.accept();
				Connection c = new Connection(clientSocket);
				clientes.add(c);
				System.out.println("Cliente adicionado");
			}
		} catch(IOException e) {System.out.println("Listen socket:"+e.getMessage());}
	}

	
}

class Connection extends Thread {
	DataInputStream in;
	DataOutputStream out;
	Socket clientSocket;
	private String mensagem;
	
	
	public Connection (Socket aClientSocket) {
		try {
			clientSocket = aClientSocket;
			in = new DataInputStream( clientSocket.getInputStream());
			out = new DataOutputStream( clientSocket.getOutputStream());
			this.start();
		} catch(IOException e) {System.out.println("Connection:"+e.getMessage());}
	}
	
	public void run(){
		while(true){
			try {			                 
				this.mensagem = in.readUTF();	// Espera mensagem do Servidor
				List<Connection> clientes = ChatServer.clientes;
				
				for(Connection cliente : clientes){
					cliente.enviarMensagem(cliente.getOut(), mensagem);
				}
				
			}catch (EOFException e){System.out.println("EOF:"+e.getMessage());
			} catch(IOException e) {
				System.out.println("readline:"+e.getMessage());
			}
		}
	}
	
	public DataOutputStream getOut(){
		return this.out;
	}
	/*
	public void notifyObservers(String mensagem) {
		this.observer.update(server, mensagem);
	}
	*/
	
	public void enviarMensagem(DataOutputStream out, String mensagem){
		try {
			out.writeUTF(mensagem);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}